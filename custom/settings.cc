
#include "../custom/settings.h"

#include "chrome/common/pref_names.h"
#include "components/safe_browsing/common/safe_browsing_prefs.h"

#include "components/policy/policy_constants.h"
#include "components/policy/core/common/policy_types.h"

namespace Custom {

void Settings::SetMandatoryPolicy(policy::PolicyMap& policies, const char* key, std::unique_ptr<base::Value> value) {
    policies.Set(key, policy::POLICY_LEVEL_MANDATORY, policy::POLICY_SCOPE_MACHINE, policy::POLICY_SOURCE_PLATFORM, std::move(value), nullptr);
}

void Settings::SetRecommendedPolicy(policy::PolicyMap& policies, const char* key, std::unique_ptr<base::Value> value) {
    policies.Set(key, policy::POLICY_LEVEL_RECOMMENDED, policy::POLICY_SCOPE_MACHINE, policy::POLICY_SOURCE_PLATFORM, std::move(value), nullptr);
}

void Settings::SetPreference(WriteablePrefStore& prefs, const char* key, std::unique_ptr<base::Value> value) {
    prefs.SetValue(key, std::move(value), WriteablePrefStore::DEFAULT_PREF_WRITE_FLAGS);
}

void Settings::ApplyPolicy(policy::PolicyMap& policies) {
    std::unique_ptr<base::DictionaryValue> extensionSettings(new base::DictionaryValue);

    extensionSettings->Set("cjpalhdlnbpafiamejdnhcphjbkeiagm.installation_mode", std::make_unique<base::Value>("normal_installed"));
    extensionSettings->Set("cjpalhdlnbpafiamejdnhcphjbkeiagm.update_url", std::make_unique<base::Value>("https://clients2.google.com/service/update2/crx"));
    extensionSettings->Set("pgdnlhfefecpicbbihgmbmffkjpaplco.installation_mode", std::make_unique<base::Value>("normal_installed"));
    extensionSettings->Set("pgdnlhfefecpicbbihgmbmffkjpaplco.update_url", std::make_unique<base::Value>("https://clients2.google.com/service/update2/crx"));
    extensionSettings->Set("kcpnkledgcbobhkgimpbmejgockkplob.installation_mode", std::make_unique<base::Value>("normal_installed"));
    extensionSettings->Set("kcpnkledgcbobhkgimpbmejgockkplob.update_url", std::make_unique<base::Value>("https://clients2.google.com/service/update2/crx"));

    SetMandatoryPolicy(policies, policy::key::kExtensionSettings, std::move(extensionSettings));

    SetMandatoryPolicy(policies, policy::key::kBackgroundModeEnabled, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kCloudPrintProxyEnabled, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kCloudPrintSubmitEnabled, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kEnableMediaRouter, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kMetricsReportingEnabled, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kNetworkPredictionOptions, std::make_unique<base::Value>(2));

    SetMandatoryPolicy(policies, policy::key::kAlternateErrorPagesEnabled, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kAutoFillEnabled, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kAutofillAddressEnabled, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kAutofillCreditCardEnabled, std::make_unique<base::Value>(false));
    SetRecommendedPolicy(policies, policy::key::kBlockThirdPartyCookies, std::make_unique<base::Value>(true));
    SetMandatoryPolicy(policies, policy::key::kPasswordManagerEnabled, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kChromeCleanupReportingEnabled, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kPromotionalTabsEnabled, std::make_unique<base::Value>(false));
    SetRecommendedPolicy(policies, policy::key::kSafeBrowsingEnabled, std::make_unique<base::Value>(true));
    SetMandatoryPolicy(policies, policy::key::kSafeBrowsingExtendedReportingEnabled, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kSpellCheckServiceEnabled, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kTranslateEnabled, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kUrlKeyedAnonymizedDataCollectionEnabled, std::make_unique<base::Value>(false));

    SetMandatoryPolicy(policies, policy::key::kImportAutofillFormData, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kImportBookmarks, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kImportHistory, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kImportHomepage, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kImportSavedPasswords, std::make_unique<base::Value>(false));
    SetMandatoryPolicy(policies, policy::key::kImportSearchEngine, std::make_unique<base::Value>(false));
}

void Settings::ApplyPreferences(WriteablePrefStore& prefs) {
    SetPreference(prefs, prefs::kEnableDoNotTrack, std::make_unique<base::Value>(true));
    SetPreference(prefs, prefs::kEnableHyperlinkAuditing, std::make_unique<base::Value>(false));
    SetPreference(prefs, prefs::kSignInPromoUserSkipped, std::make_unique<base::Value>(true));
    SetPreference(prefs, prefs::kSignInPromoShowOnFirstRunAllowed, std::make_unique<base::Value>(false));
}

}
