
#include "components/policy/core/common/policy_map.h"
#include "components/prefs/writeable_pref_store.h"

namespace Custom {

class Settings {
    private:
        static void SetMandatoryPolicy(policy::PolicyMap& policies, const char* key, std::unique_ptr<base::Value> value);
        static void SetRecommendedPolicy(policy::PolicyMap& policies, const char* key, std::unique_ptr<base::Value> value);
        static void SetPreference(WriteablePrefStore& prefs, const char* key, std::unique_ptr<base::Value> value);

    public:
        static void ApplyPolicy(policy::PolicyMap& policies);
        static void ApplyPreferences(WriteablePrefStore& prefs);
};

}
