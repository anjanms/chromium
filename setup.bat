@echo off

set PATH=%CD%\depot_tools;%PATH%;%PROGRAMFILES%\Git\mingw64\bin

set DEPOT_TOOLS_WIN_TOOLCHAIN=0

if not exist "%PROGRAMFILES%\Git\mingw64\bin\" (
    echo Missing Git MingW64
    exit /B 1
)

if not exist "%CD%\depot_tools\" (
    echo Downloading Depot Tools
    call git clone --branch master --depth 1 "https://chromium.googlesource.com/chromium/tools/depot_tools.git" ./depot_tools/ || exit /B %ERRORLEVEL%

    echo Setting Up Depot Tools
    call gclient --version || exit /B %ERRORLEVEL%
)

if not exist "%CD%\src\" (
    echo Creating Chromium Repo

    mkdir src || exit /B %ERRORLEVEL%
    pushd src || exit /B %ERRORLEVEL%

    call git init || exit /B %ERRORLEVEL%
    call git remote add origin "https://chromium.googlesource.com/chromium/src" || exit /B %ERRORLEVEL%

    popd
)
