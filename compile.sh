#!/bin/sh

source setup.sh || exit 1

cd src || exit 1

echo Cleaning Working Directory

git reset --hard || exit 1

echo Applying Patches

find ../patches -name '*.patch' -type f -print0 | while read -d '' -r patch
do
    echo Applying $patch
    git apply $patch || exit 1
done || exit 1

echo Generating Build Files

mkdir -p out/Release || exit 1

paste -s -d '\n' ../arguments/*.txt | grep -v '^#' - > out/Release/args.gn || exit 1

gn gen out/Release || exit 1

echo Compiling Chromium

ninja -C out/Release chrome || exit 1

echo Creating Disk Image

DMG_TMP=`mktemp -d` || exit 1

cp -a out/Release/Chromium.app $DMG_TMP || exit 1

ln -s /Applications $DMG_TMP/Applications || exit 1

hdiutil create -volname 'Chromium' -srcfolder $DMG_TMP -ov -format UDBZ out/Release/Chromium.dmg || exit 1

rm -r $DMG_TMP

echo Compile Complete!
