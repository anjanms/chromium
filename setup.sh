#!/bin/sh

export PATH=`pwd`/depot_tools:"$PATH"

if [ ! -e 'depot_tools' ]; then
    echo 'Downloading Depot Tools'
    git clone --branch master --depth 1 'https://chromium.googlesource.com/chromium/tools/depot_tools.git' ./depot_tools/ || exit 1

    echo 'Setting Up Depot Tools'
    gclient --version || exit 1
fi

if [ ! -e 'src' ]; then
    echo 'Creating Chromium Repo'

    mkdir src || exit 1
    pushd src > /dev/null || exit 1

    git init || exit 1
    git remote add origin 'https://chromium.googlesource.com/chromium/src' || exit 1

    popd > /dev/null
fi
