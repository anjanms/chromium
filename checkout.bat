@echo off

setlocal

call setup.bat || exit /B %ERRORLEVEL%

cd src || exit /B %ERRORLEVEL%

for /F "delims=, tokens=3,9" %%a in ('curl -s "http://omahaproxy.appspot.com/all?os=win&channel=stable"') do (
    set VERSION=%%a
    set COMMIT=%%b
)

echo Chromium %VERSION% at %COMMIT%

call git cat-file -e %COMMIT% || (
    echo Downloading %COMMIT%
    call git fetch --depth 2 origin %COMMIT% || exit /B %ERRORLEVEL%
)

echo Checking out %COMMIT%

call git checkout --force %COMMIT% || exit /B %ERRORLEVEL%

echo Syncing projects for Chromium %VERSION%

call gclient sync --with_branch_heads --no-history || exit /B %ERRORLEVEL%

echo Checkout Complete!
