@echo off

setlocal

call setup.bat || exit /B %ERRORLEVEL%

cd src || exit /B %ERRORLEVEL%

echo Cleaning Working Directory

call git reset --hard || exit /B %ERRORLEVEL%

echo Applying Patches

for /R "..\patches" %%p in (*.patch) do (
    echo Applying %%p
    call git apply "%%p" || exit /B %ERRORLEVEL%
)

echo Generating Build Files

if not exist "%CD%\out\Release" md "%CD%\out\Release"

echo. > "%CD%\out\Release\args.gn" || exit /B %ERRORLEVEL%

for /R "..\arguments" %%a in (*.txt) do (
    call type %%a 2>nul | findstr /V /B "#" >> "%CD%\out\Release\args.gn" || exit /B %ERRORLEVEL%
    echo. >> "%CD%\out\Release\args.gn" || exit /B %ERRORLEVEL%
)

call gn gen out\Release || exit /B %ERRORLEVEL%

echo Compiling Chromium

call ninja -C out\Release chrome || exit /B %ERRORLEVEL%

echo Compiling Mini Installer

call ninja -C out\Release mini_installer || exit /B %ERRORLEVEL%

echo Compile Complete!
