#!/bin/sh

source setup.sh || exit 1

cd src || exit 1

CURRENT=$(curl -s 'http://omahaproxy.appspot.com/all?os=mac&channel=stable')

VERSION=$(echo "$CURRENT" | awk -F ',' '{if (NR == 2) {print $3}}')
COMMIT=$(echo "$CURRENT" | awk -F ',' '{if (NR == 2) {print $9}}')

echo Chromium $VERSION at $COMMIT

if ! git cat-file -e "$COMMIT"; then
    echo "Downloading $COMMIT"
    git fetch --depth 2 origin "$COMMIT" || exit 1
fi

echo "Checking out $COMMIT"

git checkout --force "$COMMIT" || exit 1

echo "Syncing projects for Chromium $VERSION"

gclient sync --with_branch_heads --no-history || exit 1

echo "Checkout Complete!"
