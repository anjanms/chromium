# Chromium
##### Custom Build Script and Patches for Chromium on Mac OS and Windows

----

#### Checkout

* Downloads the latest stable Chromium for the platform and its dependencies.

#### Compile

* Applies patches and compiles Chromium with arguments.
* Creates a disk image on Mac OS and compiles Mini Installer on Windows.

#### Setup

* Sets up environment variables.
* Downloads and sets up Depot Tools if it doesn't already exist.
* Creates empty Git repository for Chromium source if it doesn't already exist.

_Note: Setup is automatically ran by both checkout and compile._

----

#### API Access

Follow [API Keys How-To](https://www.chromium.org/developers/how-tos/api-keys) to obtain API key and OAuth credentials.
Create `arguments/auth.txt` with the following content and fill in the blanks.
```
google_api_key = ""
google_default_client_id = ""
google_default_client_secret = ""
```

----

#### Useful Links

##### Mac OS

* [Chromium Build Instructions](https://chromium.googlesource.com/chromium/src/+/master/docs/mac_build_instructions.md)
* [XCode](https://itunes.apple.com/us/app/id497799835)
* [Mac OS SDKs](https://github.com/phracker/MacOSX-SDKs)

##### Windows

* [Chromium Build Instructions](https://chromium.googlesource.com/chromium/src/+/master/docs/windows_build_instructions.md)
* [Visual Studio 2017](https://imagine.microsoft.com/en-us/Catalog/Product/530)
* [Windows SDKs](https://developer.microsoft.com/en-us/windows/downloads/sdk-archive)
